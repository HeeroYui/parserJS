/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "CScriptVarLink.h"
#include "CScriptVar.h"
#include "CScriptException.h"


CScriptVarLink::CScriptVarLink(CScriptVar* _var, const std::string& _name) {
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	m_name = _name;
	m_nextSibling = 0;
	m_prevSibling = 0;
	m_var = _var->ref();
	m_owned = false;
}

CScriptVarLink::CScriptVarLink(const CScriptVarLink& _link) {
	// Copy constructor
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	m_name = _link.m_name;
	m_nextSibling = 0;
	m_prevSibling = 0;
	m_var = _link.m_var->ref();
	m_owned = false;
}

CScriptVarLink::~CScriptVarLink() {
#ifdef DEBUG_MEMORY
	mark_deallocated(this);
#endif
	m_var->unref();
}

void CScriptVarLink::replaceWith(CScriptVar* _newVar) {
	CScriptVar* oldVar = m_var;
	m_var = _newVar->ref();
	oldVar->unref();
}

void CScriptVarLink::replaceWith(CScriptVarLink* _newVar) {
	if (_newVar) {
		replaceWith(_newVar->m_var);
	} else {
		replaceWith(new CScriptVar());
	}
}

int32_t CScriptVarLink::getIntName() {
	return atoi(m_name.c_str());
}

void CScriptVarLink::setIntName(int _n) {
	char sIdx[64];
	snprintf(sIdx, sizeof(sIdx), "%d", _n);
	m_name = sIdx;
}
