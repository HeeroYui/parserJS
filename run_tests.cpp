/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

/*
 * This is a program to run all the tests in the tests folder...
 */

#include "CTinyJS.h"
#include "CScriptException.h"
#include "basic/functions.h"
#include "basic/mathFunction.h"
#include <assert.h>
#include <sys/stat.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <etk/os/FSNode.h>
#include "debug.h"

bool run_test(const std::string& _filename) {
	PARSERJS_INFO("TEST " << _filename);
	// read all data :
	std::string buffer = etk::FSNodeReadAllData(_filename);
	CTinyJS s;
	registerFunctions(&s);
	registerMathFunctions(&s);
	s.m_root->addChild("result", new CScriptVar("0",SCRIPTVAR_INTEGER));
	try {
		s.execute(&buffer[0]);
	} catch (CScriptException *e) {
		PARSERJS_ERROR(e->what());
	}
	bool pass = s.m_root->getParameter("result")->getBool();
	if (pass) {
		PARSERJS_INFO("PASS");
	} else {
		std::ostringstream symbols;
		s.m_root->getJSON(symbols);
		etk::FSNodeEcho(_filename + ".fail.json", symbols.str());
		PARSERJS_ERROR("FAIL - symbols written to " << _filename << ".fail.json");
	}
	return pass;
}


//const char *code = "var a = 5; if (a==5) a=4; else a=3;";
//const char *code = "{ var a = 4; var b = 1; while (a>0) { b = b * 2; a = a - 1; } var c = 5; }";
//const char *code = "{ var b = 1; for (var i=0;i<4;i=i+1) b = b * 2; }";
const char *code = "function myfunc(x, y) { return x + y; } var a = myfunc(1,2); print(a);";

void js_print(CScriptVar *v, void *userdata) {
	std::cout << v->getParameter("text")->getString() << std::endl;
}

void js_dump(CScriptVar *_v, void *_userdata) {
	CTinyJS *js = (CTinyJS*)_userdata;
	js->m_root->trace("#\t");
}

void run_interactive() {
	PARSERJS_INFO("Run interactive");
	CTinyJS js;
	/* Add a native function */
	js.addNative("function print(text)", &js_print, 0);
	js.addNative("function dump()", &js_dump, &js);
	
	registerFunctions(&js);
	registerMathFunctions(&js);
	
	/* Execute out bit of code - we could call 'evaluate' here if we wanted something returned */
	try {
		js.execute("var lets_quit = 0; function quit() { lets_quit = 1; }");
		js.execute("print(\"Interactive mode... \n\tquit(); to exit\n\tprint(...); to print something\n\tdump() to dump the symbol table!\n\");");
	} catch (CScriptException *e) {
		PARSERJS_ERROR(e->what());
	}
	while (js.evaluate("lets_quit") == "0") {
		std::cout << ">> ";
		fflush(stdout);
		char buffer[2048];
		fgets(buffer, sizeof(buffer), stdin );
		try {
			js.execute(buffer);
		} catch (CScriptException *e) {
			PARSERJS_ERROR(e->what());
		}
	}
}

int main(int argc, char **argv) {
	// the only one init for etk:
	etk::log::setLevel(etk::log::logLevelNone);
	std::vector<std::string> listFileToTest;
	for (int32_t iii=1; iii<argc ; ++iii) {
		std::string data = argv[iii];
		if (data == "-l0") {
			etk::log::setLevel(etk::log::logLevelNone);
		} else if (data == "-l1") {
			etk::log::setLevel(etk::log::logLevelCritical);
		} else if (data == "-l2") {
			etk::log::setLevel(etk::log::logLevelError);
		} else if (data == "-l3") {
			etk::log::setLevel(etk::log::logLevelWarning);
		} else if (data == "-l4") {
			etk::log::setLevel(etk::log::logLevelInfo);
		} else if (data == "-l5") {
			etk::log::setLevel(etk::log::logLevelDebug);
		} else if (data == "-l6") {
			etk::log::setLevel(etk::log::logLevelVerbose);
		} else if (    data == "-h"
		            || data == "--help") {
			PARSERJS_INFO("Help : ");
			PARSERJS_INFO("    ./xxx [options]");
			PARSERJS_INFO("        -l0: debug None");
			PARSERJS_INFO("        -l1: debug Critical");
			PARSERJS_INFO("        -l2: debug Error");
			PARSERJS_INFO("        -l3: debug Warning");
			PARSERJS_INFO("        -l4: debug Info");
			PARSERJS_INFO("        -l5: debug Debug");
			PARSERJS_INFO("        -l6: debug Verbose");
			PARSERJS_INFO("        -h/--help: this help");
			exit(0);
		} else {
			listFileToTest.push_back(data);
		}
	}
	etk::setArgZero(argv[0]);
	etk::initDefaultFolder("exml_test");
	PARSERJS_INFO("input elements : " << listFileToTest);
	// Cocal parse :
	if (listFileToTest.size() == 0) {
		run_interactive();
	} else {
		int32_t test_num = 1;
		int32_t count = 0;
		int32_t passed = 0;
		for (auto &it : listFileToTest) {
			enum etk::typeNode type = etk::FSNode(it).getNodeType();
			if (type == etk::FSN_FOLDER) {
				etk::FSNode node(it);
				std::vector<std::string> list;
				node.folderGetRecursiveFiles(list, false);
				for (auto &it2 : list) {
					if (run_test(it2)) {
						passed++;
					}
					count++;
					test_num++;
				}
			} else if (type == etk::FSN_FILE) {
				if (run_test(it)) {
					passed++;
				}
				count++;
				test_num++;
			}
		}
		PARSERJS_INFO("Done. " << count << " tests, " << passed << " pass, " << count-passed << " fail");
	}
	return 0;
}
