/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_TOOLS_H__
#define __PARSERJS_TOOLS_H__

#include <etk/types.h>
#include <string>
#include <string.h>
#include <sstream>
#include <cstdlib>
#include <stdio.h>
#include <assert.h>

// If defined, this keeps a note of all calls and where from in memory. This is slower, but good for debugging
#define TINYJS_CALL_STACK
#define DEBUG_MEMORY


#include <string>
#include <vector>

const int TINYJS_LOOP_MAX_ITERATIONS = 8192;

enum LEX_TYPES {
	LEX_EOF = 0,
	LEX_ID = 256,
	LEX_INT,
	LEX_FLOAT,
	LEX_STR,

	LEX_EQUAL,
	LEX_TYPEEQUAL,
	LEX_NEQUAL,
	LEX_NTYPEEQUAL,
	LEX_LEQUAL,
	LEX_LSHIFT,
	LEX_LSHIFTEQUAL,
	LEX_GEQUAL,
	LEX_RSHIFT,
	LEX_RSHIFTUNSIGNED,
	LEX_RSHIFTEQUAL,
	LEX_PLUSEQUAL,
	LEX_MINUSEQUAL,
	LEX_PLUSPLUS,
	LEX_MINUSMINUS,
	LEX_ANDEQUAL,
	LEX_ANDAND,
	LEX_OREQUAL,
	LEX_OROR,
	LEX_XOREQUAL,
	// reserved words
#define LEX_R_LIST_START LEX_R_IF
	LEX_R_IF,
	LEX_R_ELSE,
	LEX_R_DO,
	LEX_R_WHILE,
	LEX_R_FOR,
	LEX_R_BREAK,
	LEX_R_CONTINUE,
	LEX_R_FUNCTION,
	LEX_R_RETURN,
	LEX_R_VAR,
	LEX_R_TRUE,
	LEX_R_FALSE,
	LEX_R_NULL,
	LEX_R_UNDEFINED,
	LEX_R_NEW,

	LEX_R_LIST_END /* always the last entry */
};

enum SCRIPTVAR_FLAGS {
	SCRIPTVAR_UNDEFINED = 0,
	SCRIPTVAR_FUNCTION = 1,
	SCRIPTVAR_OBJECT = 2,
	SCRIPTVAR_ARRAY = 4,
	SCRIPTVAR_DOUBLE = 8,  // floating point double
	SCRIPTVAR_INTEGER = 16, // integer number
	SCRIPTVAR_STRING = 32, // std::string
	SCRIPTVAR_NULL = 64, // it seems null is its own data type

	SCRIPTVAR_NATIVE = 128, // to specify this is a native function
	SCRIPTVAR_NUMERICMASK = SCRIPTVAR_NULL |
							SCRIPTVAR_DOUBLE |
							SCRIPTVAR_INTEGER,
	SCRIPTVAR_VARTYPEMASK = SCRIPTVAR_DOUBLE |
							SCRIPTVAR_INTEGER |
							SCRIPTVAR_STRING |
							SCRIPTVAR_FUNCTION |
							SCRIPTVAR_OBJECT |
							SCRIPTVAR_ARRAY |
							SCRIPTVAR_NULL,

};

#define TINYJS_RETURN_VAR "return"
#define TINYJS_PROTOTYPE_CLASS "prototype"
#define TINYJS_TEMP_NAME ""
#define TINYJS_BLANK_DATA ""

/// convert the given std::string into a quoted std::string suitable for javascript
std::string getJSString(const std::string &str);


/* Frees the given link IF it isn't owned by anything else */
#define CLEAN(x) do { \
		CScriptVarLink *__v = x; \
		if (__v && !__v->m_owned) { \
			delete __v; \
		} \
	} while (false)
/* Create a LINK to point to VAR and free the old link.
 * BUT this is more clever - it tries to keep the old link if it's not owned to save allocations */
#define CREATE_LINK(LINK, VAR) do { \
		if (!LINK || LINK->m_owned) \
			LINK = new CScriptVarLink(VAR); \
		else \
			LINK->replaceWith(VAR); \
		} while (false)



#ifdef DEBUG_MEMORY
class CScriptVar;
class CScriptVarLink;
void mark_allocated(CScriptVar *v);
void mark_deallocated(CScriptVar *v);
void mark_allocated(CScriptVarLink *v);
void mark_deallocated(CScriptVarLink *v);
void show_allocated();
#endif

bool isWhitespace(char ch);
bool isNumeric(char ch);
bool isNumber(const std::string &str);
bool isHexadecimal(char ch);
bool isAlpha(char ch);
bool isIDString(const char *s);
void replace(std::string &str, char textFrom, const char *textTo);
std::string getJSString(const std::string &str) ;
bool isAlphaNum(const std::string &str);


#endif
