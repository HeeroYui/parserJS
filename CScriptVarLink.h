/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_C_SCRIPT_VAR_LINK_H__
#define __PARSERJS_C_SCRIPT_VAR_LINK_H__

#include "tools.h"

class CScriptVar;
class CScriptVarLink {
	public:
		std::string m_name;
		CScriptVarLink* m_nextSibling;
		CScriptVarLink* m_prevSibling;
		CScriptVar* m_var;
		bool m_owned;
		CScriptVarLink(CScriptVar* _var, const std::string& _name = TINYJS_TEMP_NAME);
		CScriptVarLink(const CScriptVarLink& _link); ///< Copy constructor
		~CScriptVarLink();
		void replaceWith(CScriptVar* _newVar); ///< Replace the Variable pointed to
		void replaceWith(CScriptVarLink* _newVar); ///< Replace the Variable pointed to (just dereferences)
		int32_t getIntName(); ///< Get the name as an integer (for arrays)
		void setIntName(int32_t _n); ///< Set the name as an integer (for arrays)
};


#endif
