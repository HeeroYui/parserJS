/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_DEBUG_H__
#define __PARSERJS_DEBUG_H__

#include <etk/log.h>

namespace parserJS {
	int32_t getLogId();
};
// TODO : Review this problem of multiple intanciation of "std::stringbuf sb"
#define PARSERJS_BASE(info,data) \
	do { \
		if (info <= etk::log::getLevel(parserJS::getLogId())) { \
			std::stringbuf sb; \
			std::ostream tmpStream(&sb); \
			tmpStream << data; \
			etk::log::logStream(parserJS::getLogId(), info, __LINE__, __class__, __func__, tmpStream); \
		} \
	} while(0)

#define PARSERJS_CRITICAL(data)      PARSERJS_BASE(1, data)
#define PARSERJS_ERROR(data)         PARSERJS_BASE(2, data)
#define PARSERJS_WARNING(data)       PARSERJS_BASE(3, data)
#ifdef DEBUG
	#define PARSERJS_INFO(data)          PARSERJS_BASE(4, data)
	#define PARSERJS_DEBUG(data)         PARSERJS_BASE(5, data)
	#define PARSERJS_VERBOSE(data)       PARSERJS_BASE(6, data)
	#define PARSERJS_TODO(data)          PARSERJS_BASE(4, "TODO : " << data)
#else
	#define PARSERJS_INFO(data)          do { } while(false)
	#define PARSERJS_DEBUG(data)         do { } while(false)
	#define PARSERJS_VERBOSE(data)       do { } while(false)
	#define PARSERJS_TODO(data)          do { } while(false)
#endif

#define PARSERJS_ASSERT(cond,data) \
	do { \
		if (!(cond)) { \
			PARSERJS_CRITICAL(data); \
			assert(!#cond); \
		} \
	} while (0)


#endif

