/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_C_SCRIPT_LEX_H__
#define __PARSERJS_C_SCRIPT_LEX_H__

#include "tools.h"

class CScriptLex {
	public:
		CScriptLex(const std::string& _input);
		CScriptLex(CScriptLex* _owner, int _startChar, int _endChar);
		~CScriptLex(void);
		char m_currCh;
		char m_nextCh;
		int m_tk; //!< The type of the token that we have
		int m_tokenStart; //!< Position in the data at the beginning of the token we have here
		int m_tokenEnd; //!< Position in the data at the last character of the token we have here
		int m_tokenLastEnd; //!< Position in the data at the last character of the last token
		std::string m_tkStr; //!< Data contained in the token we have here
		/**
		 * @brief Lexical match wotsit
		 */
		void match(int32_t _expectedTk);
		/**
		 * @brief Get the string representation of the given token
		 */
		static std::string getTokenStr(int32_t _token);
		/**
		 * @brief ///< Reset this lex so we can start again
		 */
		void reset();
		/**
		 * @brief Return a sub-string from the given position up until right now
		 */
		std::string getSubString(int32_t _pos);
		/**
		 * @brief Return a sub-lexer from the given position up until right now
		 */
		CScriptLex* getSubLex(int32_t _lastPosition);
		/**
		 * @brief Return a string representing the position in lines and columns of the character pos given
		 */
		std::string getPosition(int32_t _pos=-1);
	protected:
		/* When we go into a loop, we use getSubLex to get a lexer for just the sub-part of the
		   relevant string. This doesn't re-allocate and copy the string, but instead copies
		   the data pointer and sets dataOwned to false, and dataStart/dataEnd to the relevant things. */
		char *m_data; //!< Data string to get tokens from
		int32_t m_dataStart;
		int32_t m_dataEnd; //!< Start and end position in data string
		bool m_dataOwned; //!< Do we own this data string?
		int32_t m_dataPos; //!< Position in data (we CAN go past the end of the string here)
		void getNextCh();
		void getNextToken(); //!< Get the text token from our text string
};



#endif
