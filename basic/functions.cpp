/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "functions.h"
#include <math.h>
#include <cstdlib>
#include <sstream>
#include "../CScriptVar.h"
#include "../CScriptVarLink.h"

using namespace std;
// ----------------------------------------------- Actual Functions
void scTrace(CScriptVar* _c, void* _userdata) {
	CTinyJS *js = (CTinyJS*)_userdata;
	js->m_root->trace();
}

void scObjectDump(CScriptVar* _c, void* _userdata) {
	_c->getParameter("this")->trace("> ");
}

void scObjectClone(CScriptVar* _c, void* _userdata) {
	CScriptVar *obj = _c->getParameter("this");
	_c->getReturnVar()->copyValue(obj);
}

void scMathRand(CScriptVar* _c, void* _userdata) {
	_c->getReturnVar()->setDouble((double)rand()/RAND_MAX);
}

void scMathRandInt(CScriptVar* _c, void* _userdata) {
	int min = _c->getParameter("min")->getInt();
	int max = _c->getParameter("max")->getInt();
	int val = min + (int)(rand()%(1+max-min));
	_c->getReturnVar()->setInt(val);
}

void scCharToInt(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("ch")->getString();;
	int val = 0;
	if (str.length()>0)
			val = (int)str.c_str()[0];
	_c->getReturnVar()->setInt(val);
}

void scStringIndexOf(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("this")->getString();
	string search = _c->getParameter("search")->getString();
	size_t p = str.find(search);
	int val = (p==string::npos) ? -1 : p;
	_c->getReturnVar()->setInt(val);
}

void scStringSubstring(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("this")->getString();
	int lo = _c->getParameter("lo")->getInt();
	int hi = _c->getParameter("hi")->getInt();

	int l = hi-lo;
	if (l>0 && lo>=0 && lo+l<=(int)str.length()) {
		_c->getReturnVar()->setString(str.substr(lo, l));
	} else {
		_c->getReturnVar()->setString("");
	}
}

void scStringCharAt(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("this")->getString();
	int p = _c->getParameter("pos")->getInt();
	if (p>=0 && p<(int)str.length()) {
		_c->getReturnVar()->setString(str.substr(p, 1));
	} else {
		_c->getReturnVar()->setString("");
	}
}

void scStringCharCodeAt(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("this")->getString();
	int p = _c->getParameter("pos")->getInt();
	if (p>=0 && p<(int)str.length()) {
		_c->getReturnVar()->setInt(str.at(p));
	} else {
		_c->getReturnVar()->setInt(0);
	}
}

void scStringSplit(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("this")->getString();
	string sep = _c->getParameter("separator")->getString();
	CScriptVar *result = _c->getReturnVar();
	result->setArray();
	int length = 0;
	size_t pos = str.find(sep);
	while (pos != string::npos) {
		result->setArrayIndex(length++, new CScriptVar(str.substr(0,pos)));
		str = str.substr(pos+1);
		pos = str.find(sep);
	}
	if (str.size()>0) {
		result->setArrayIndex(length++, new CScriptVar(str));
	}
}

void scStringFromCharCode(CScriptVar* _c, void* _userdata) {
	char str[2];
	str[0] = _c->getParameter("char")->getInt();
	str[1] = 0;
	_c->getReturnVar()->setString(str);
}

void scIntegerParseInt(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("str")->getString();
	int val = strtol(str.c_str(),0,0);
	_c->getReturnVar()->setInt(val);
}

void scIntegerValueOf(CScriptVar* _c, void* _userdata) {
	string str = _c->getParameter("str")->getString();
	int val = 0;
	if (str.length()==1) {
		val = str[0];
	}
	_c->getReturnVar()->setInt(val);
}

void scJSONStringify(CScriptVar* _c, void* _userdata) {
	std::ostringstream result;
	_c->getParameter("obj")->getJSON(result);
	_c->getReturnVar()->setString(result.str());
}

void scExec(CScriptVar* _c, void* _userdata) {
	CTinyJS *tinyJS = (CTinyJS *)_userdata;
	std::string str = _c->getParameter("jsCode")->getString();
	tinyJS->execute(str);
}

void scEval(CScriptVar* _c, void* _userdata) {
	CTinyJS *tinyJS = (CTinyJS *)_userdata;
	std::string str = _c->getParameter("jsCode")->getString();
	_c->setReturnVar(tinyJS->evaluateComplex(str).m_var);
}

void scArrayContains(CScriptVar* _c, void* _userdata) {
	CScriptVar *obj = _c->getParameter("obj");
	CScriptVarLink *v = _c->getParameter("this")->m_firstChild;
	bool contains = false;
	while (v) {
		if (v->m_var->equals(obj)) {
			contains = true;
			break;
		}
		v = v->m_nextSibling;
	}
	_c->getReturnVar()->setInt(contains);
}

void scArrayRemove(CScriptVar* _c, void* _userdata) {
	CScriptVar *obj = _c->getParameter("obj");
	vector<int> removedIndices;
	CScriptVarLink *v;
	// remove
	v = _c->getParameter("this")->m_firstChild;
	while (v) {
		if (v->m_var->equals(obj)) {
			removedIndices.push_back(v->getIntName());
		}
		v = v->m_nextSibling;
	}
	// renumber
	v = _c->getParameter("this")->m_firstChild;
	while (v) {
		int n = v->getIntName();
		int newn = n;
		for (size_t i=0;i<removedIndices.size();i++) {
			if (n>=removedIndices[i]) {
				newn--;
			}
		}
		if (newn!=n) {
			v->setIntName(newn);
		}
		v = v->m_nextSibling;
	}
}

void scArrayJoin(CScriptVar* _c, void* _userdata) {
	string sep = _c->getParameter("separator")->getString();
	CScriptVar *arr = _c->getParameter("this");
	ostringstream sstr;
	int l = arr->getArrayLength();
	for (int i=0;i<l;i++) {
		if (i>0) {
			sstr << sep;
		}
		sstr << arr->getArrayIndex(i)->getString();
	}
	_c->getReturnVar()->setString(sstr.str());
}

// ----------------------------------------------- Register Functions
void registerFunctions(CTinyJS *tinyJS) {
	tinyJS->addNative("function exec(jsCode)", scExec, tinyJS); // execute the given code
	tinyJS->addNative("function eval(jsCode)", scEval, tinyJS); // execute the given string (an expression) and return the result
	tinyJS->addNative("function trace()", scTrace, tinyJS);
	tinyJS->addNative("function Object.dump()", scObjectDump, 0);
	tinyJS->addNative("function Object.clone()", scObjectClone, 0);
	tinyJS->addNative("function Math.rand()", scMathRand, 0);
	tinyJS->addNative("function Math.randInt(min, max)", scMathRandInt, 0);
	tinyJS->addNative("function charToInt(ch)", scCharToInt, 0); //	convert a character to an int - get its value
	tinyJS->addNative("function String.indexOf(search)", scStringIndexOf, 0); // find the position of a string in a string, -1 if not
	tinyJS->addNative("function String.substring(lo,hi)", scStringSubstring, 0);
	tinyJS->addNative("function String.charAt(pos)", scStringCharAt, 0);
	tinyJS->addNative("function String.charCodeAt(pos)", scStringCharCodeAt, 0);
	tinyJS->addNative("function String.fromCharCode(char)", scStringFromCharCode, 0);
	tinyJS->addNative("function String.split(separator)", scStringSplit, 0);
	tinyJS->addNative("function Integer.parseInt(str)", scIntegerParseInt, 0); // string to int
	tinyJS->addNative("function Integer.valueOf(str)", scIntegerValueOf, 0); // value of a single character
	tinyJS->addNative("function JSON.stringify(obj, replacer)", scJSONStringify, 0); // convert to JSON. replacer is ignored at the moment
	// JSON.parse is left out as you can (unsafely!) use eval instead
	tinyJS->addNative("function Array.contains(obj)", scArrayContains, 0);
	tinyJS->addNative("function Array.remove(obj)", scArrayRemove, 0);
	tinyJS->addNative("function Array.join(separator)", scArrayJoin, 0);
}

