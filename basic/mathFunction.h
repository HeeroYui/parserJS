/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSER_JS_MATH_FUNCTIONS_H__
#define __PARSER_JS_MATH_FUNCTIONS_H__

#include "../CTinyJS.h"

/// Register useful math. functions with the TinyJS interpreter
extern void registerMathFunctions(CTinyJS* _tinyJS);

#endif
