/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "CScriptLex.h"
#include "CScriptException.h"
#include "tools.h"
#include "debug.h"

CScriptLex::CScriptLex(const std::string& _input) {
	m_data = strdup(_input.c_str());
	m_dataOwned = true;
	m_dataStart = 0;
	m_dataEnd = strlen(m_data);
	PARSERJS_VERBOSE("create lexer with : '" << _input << "'");
	reset();
}

CScriptLex::CScriptLex(CScriptLex* _owner, int _startChar, int _endChar) {
	PARSERJS_VERBOSE("create sub lexer with : '" << _owner->m_data << "'");
	m_data = _owner->m_data;
	m_dataOwned = false;
	m_dataStart = _startChar;
	m_dataEnd = _endChar;
	reset();
}

CScriptLex::~CScriptLex(void) {
	if (m_dataOwned) {
		free((void*)m_data);
		m_data = nullptr;
	}
	PARSERJS_VERBOSE("remove lexer");
}

void CScriptLex::reset() {
	m_dataPos = m_dataStart;
	m_tokenStart = 0;
	m_tokenEnd = 0;
	m_tokenLastEnd = 0;
	m_tk = 0;
	m_tkStr = "";
	getNextCh();
	getNextCh();
	getNextToken();
}

void CScriptLex::match(int32_t _expected_tk) {
	if (m_tk != _expected_tk) {
		std::ostringstream errorString;
		errorString << "Got " << getTokenStr(m_tk) << " expected " << getTokenStr(_expected_tk)
		 << " at " << getPosition(m_tokenStart);
		throw new CScriptException(errorString.str());
	}
	getNextToken();
}

std::string CScriptLex::getTokenStr(int32_t _token) {
	if (_token>32 && _token<128) {
		char buf[4] = "' '";
		buf[1] = (char)_token;
		return buf;
	}
	switch (_token) {
		case LEX_EOF: return "EOF";
		case LEX_ID: return "ID";
		case LEX_INT: return "INT";
		case LEX_FLOAT: return "FLOAT";
		case LEX_STR: return "STRING";
		case LEX_EQUAL: return "==";
		case LEX_TYPEEQUAL: return "===";
		case LEX_NEQUAL: return "!=";
		case LEX_NTYPEEQUAL: return "!==";
		case LEX_LEQUAL: return "<=";
		case LEX_LSHIFT: return "<<";
		case LEX_LSHIFTEQUAL: return "<<=";
		case LEX_GEQUAL: return ">=";
		case LEX_RSHIFT: return ">>";
		case LEX_RSHIFTUNSIGNED: return ">>";
		case LEX_RSHIFTEQUAL: return ">>=";
		case LEX_PLUSEQUAL: return "+=";
		case LEX_MINUSEQUAL: return "-=";
		case LEX_PLUSPLUS: return "++";
		case LEX_MINUSMINUS: return "--";
		case LEX_ANDEQUAL: return "&=";
		case LEX_ANDAND: return "&&";
		case LEX_OREQUAL: return "|=";
		case LEX_OROR: return "||";
		case LEX_XOREQUAL: return "^=";
		// reserved words
		case LEX_R_IF: return "if";
		case LEX_R_ELSE: return "else";
		case LEX_R_DO: return "do";
		case LEX_R_WHILE: return "while";
		case LEX_R_FOR: return "for";
		case LEX_R_BREAK: return "break";
		case LEX_R_CONTINUE: return "continue";
		case LEX_R_FUNCTION: return "function";
		case LEX_R_RETURN: return "return";
		case LEX_R_VAR: return "var";
		case LEX_R_TRUE: return "true";
		case LEX_R_FALSE: return "false";
		case LEX_R_NULL: return "null";
		case LEX_R_UNDEFINED: return "undefined";
		case LEX_R_NEW: return "new";
	}
	std::ostringstream msg;
	msg << "?[" << _token << "]";
	return msg.str();
}

void CScriptLex::getNextCh() {
	m_currCh = m_nextCh;
	if (m_dataPos < m_dataEnd) {
		m_nextCh = m_data[m_dataPos];
	} else {
		m_nextCh = 0;
	}
	m_dataPos++;
}

void CScriptLex::getNextToken() {
	m_tk = LEX_EOF;
	m_tkStr.clear();
	while (    m_currCh
	        && isWhitespace(m_currCh)) getNextCh();
	// newline comments
	if (m_currCh=='/' && m_nextCh=='/') {
		while (m_currCh && m_currCh!='\n') {
			getNextCh();
		}
		getNextCh();
		getNextToken();
		return;
	}
	// block comments
	if (m_currCh=='/' && m_nextCh=='*') {
		while (m_currCh && (m_currCh!='*' || m_nextCh!='/')) {
			getNextCh();
		}
		getNextCh();
		getNextCh();
		getNextToken();
		return;
	}
	// record beginning of this token
	m_tokenStart = m_dataPos-2;
	// tokens
	if (isAlpha(m_currCh)) {
		//	IDs
		while (isAlpha(m_currCh) || isNumeric(m_currCh)) {
			m_tkStr += m_currCh;
			getNextCh();
		}
		m_tk = LEX_ID;
		if (m_tkStr=="if") m_tk = LEX_R_IF;
		else if (m_tkStr=="else") m_tk = LEX_R_ELSE;
		else if (m_tkStr=="do") m_tk = LEX_R_DO;
		else if (m_tkStr=="while") m_tk = LEX_R_WHILE;
		else if (m_tkStr=="for") m_tk = LEX_R_FOR;
		else if (m_tkStr=="break") m_tk = LEX_R_BREAK;
		else if (m_tkStr=="continue") m_tk = LEX_R_CONTINUE;
		else if (m_tkStr=="function") m_tk = LEX_R_FUNCTION;
		else if (m_tkStr=="return") m_tk = LEX_R_RETURN;
		else if (m_tkStr=="var") m_tk = LEX_R_VAR;
		else if (m_tkStr=="true") m_tk = LEX_R_TRUE;
		else if (m_tkStr=="false") m_tk = LEX_R_FALSE;
		else if (m_tkStr=="null") m_tk = LEX_R_NULL;
		else if (m_tkStr=="undefined") m_tk = LEX_R_UNDEFINED;
		else if (m_tkStr=="new") m_tk = LEX_R_NEW;
	} else if (isNumeric(m_currCh)) { // Numbers
		bool isHex = false;
		if (m_currCh=='0') {
			m_tkStr += m_currCh;
			getNextCh();
		}
		if (m_currCh=='x') {
			isHex = true;
			m_tkStr += m_currCh;
			getNextCh();
		}
		m_tk = LEX_INT;
		while (isNumeric(m_currCh) || (isHex == true && isHexadecimal(m_currCh))) {
			m_tkStr += m_currCh;
			getNextCh();
		}
		if (isHex == false && m_currCh=='.') {
			m_tk = LEX_FLOAT;
			m_tkStr += '.';
			getNextCh();
			while (isNumeric(m_currCh)) {
				m_tkStr += m_currCh;
				getNextCh();
			}
		}
		// do fancy e-style floating point
		if (isHex == false && (m_currCh=='e'||m_currCh=='E')) {
			m_tk = LEX_FLOAT;
			m_tkStr += m_currCh;
			getNextCh();
			if (m_currCh=='-') {
				m_tkStr += m_currCh;
				getNextCh();
			}
			while (isNumeric(m_currCh)) {
				m_tkStr += m_currCh;
				getNextCh();
			}
		}
	} else if (m_currCh=='"') {
		// std::strings...
		getNextCh();
		while (m_currCh && m_currCh!='"') {
			if (m_currCh == '\\') {
				getNextCh();
				switch (m_currCh) {
					case 'n':
						m_tkStr += '\n';
						break;
					case '"':
						m_tkStr += '"';
						break;
					case '\\':
						m_tkStr += '\\';
						break;
					default:
						m_tkStr += m_currCh;
				}
			} else {
				m_tkStr += m_currCh;
			}
			getNextCh();
		}
		getNextCh();
		m_tk = LEX_STR;
	} else if (m_currCh=='\'') {
		// std::strings again...
		getNextCh();
		while (m_currCh && m_currCh!='\'') {
			if (m_currCh == '\\') {
				getNextCh();
				switch (m_currCh) {
					case 'n' : m_tkStr += '\n'; break;
					case 'a' : m_tkStr += '\a'; break;
					case 'r' : m_tkStr += '\r'; break;
					case 't' : m_tkStr += '\t'; break;
					case '\'' : m_tkStr += '\''; break;
					case '\\' : m_tkStr += '\\'; break;
					case 'x' : { // hex digits
						char buf[3] = "??";
						getNextCh();
						buf[0] = m_currCh;
						getNextCh();
						buf[1] = m_currCh;
						m_tkStr += (char)strtol(buf,0,16);
						}
						break;
					default:
						if (m_currCh>='0' && m_currCh<='7') {
							// octal digits
							char buf[4] = "???";
							buf[0] = m_currCh;
							getNextCh();
							buf[1] = m_currCh;
							getNextCh();
							buf[2] = m_currCh;
							m_tkStr += (char)strtol(buf,0,8);
						} else {
							m_tkStr += m_currCh;
						}
				}
			} else {
				m_tkStr += m_currCh;
			}
			getNextCh();
		}
		getNextCh();
		m_tk = LEX_STR;
	} else {
		// single chars
		m_tk = m_currCh;
		if (m_currCh) {
			getNextCh();
		}
		if (m_tk=='=' && m_currCh=='=') { // ==
			m_tk = LEX_EQUAL;
			getNextCh();
			if (m_currCh=='=') { // ===
				m_tk = LEX_TYPEEQUAL;
				getNextCh();
			}
		} else if (m_tk=='!' && m_currCh=='=') { // !=
			m_tk = LEX_NEQUAL;
			getNextCh();
			if (m_currCh=='=') { // !==
				m_tk = LEX_NTYPEEQUAL;
				getNextCh();
			}
		} else if (m_tk=='<' && m_currCh=='=') {
			m_tk = LEX_LEQUAL;
			getNextCh();
		} else if (m_tk=='<' && m_currCh=='<') {
			m_tk = LEX_LSHIFT;
			getNextCh();
			if (m_currCh=='=') { // <<=
				m_tk = LEX_LSHIFTEQUAL;
				getNextCh();
			}
		} else if (m_tk=='>' && m_currCh=='=') {
			m_tk = LEX_GEQUAL;
			getNextCh();
		} else if (m_tk=='>' && m_currCh=='>') {
			m_tk = LEX_RSHIFT;
			getNextCh();
			if (m_currCh=='=') { // >>=
				m_tk = LEX_RSHIFTEQUAL;
				getNextCh();
			} else if (m_currCh=='>') { // >>>
				m_tk = LEX_RSHIFTUNSIGNED;
				getNextCh();
			}
		} else if (m_tk=='+' && m_currCh=='=') {
			m_tk = LEX_PLUSEQUAL;
			getNextCh();
		} else if (m_tk=='-' && m_currCh=='=') {
			m_tk = LEX_MINUSEQUAL;
			getNextCh();
		} else if (m_tk=='+' && m_currCh=='+') {
			m_tk = LEX_PLUSPLUS;
			getNextCh();
		} else if (m_tk=='-' && m_currCh=='-') {
			m_tk = LEX_MINUSMINUS;
			getNextCh();
		} else if (m_tk=='&' && m_currCh=='=') {
			m_tk = LEX_ANDEQUAL;
			getNextCh();
		} else if (m_tk=='&' && m_currCh=='&') {
			m_tk = LEX_ANDAND;
			getNextCh();
		} else if (m_tk=='|' && m_currCh=='=') {
			m_tk = LEX_OREQUAL;
			getNextCh();
		} else if (m_tk=='|' && m_currCh=='|') {
			m_tk = LEX_OROR;
			getNextCh();
		} else if (m_tk=='^' && m_currCh=='=') {
			m_tk = LEX_XOREQUAL;
			getNextCh();
		}
	}
	/* This isn't quite right yet */
	m_tokenLastEnd = m_tokenEnd;
	m_tokenEnd = m_dataPos-3;
}

std::string CScriptLex::getSubString(int32_t _lastPosition) {
	int lastCharIdx = m_tokenLastEnd+1;
	if (lastCharIdx < m_dataEnd) {
		/* save a memory alloc by using our data array to create the substring */
		char old = m_data[lastCharIdx];
		m_data[lastCharIdx] = 0;
		std::string value = &m_data[_lastPosition];
		m_data[lastCharIdx] = old;
		return value;
	} else {
		return std::string(&m_data[_lastPosition]);
	}
}


CScriptLex *CScriptLex::getSubLex(int32_t _lastPosition) {
	int lastCharIdx = m_tokenLastEnd+1;
	if (lastCharIdx < m_dataEnd) {
		return new CScriptLex(this, _lastPosition, lastCharIdx);
	} else {
		return new CScriptLex(this, _lastPosition, m_dataEnd );
	}
}

std::string CScriptLex::getPosition(int32_t _pos) {
	if (_pos<0) {
		_pos=m_tokenLastEnd;
	}
	int32_t line = 1;
	int32_t col = 1;
	for (int iii=0; iii<_pos; ++iii) {
		char ch;
		if (iii < m_dataEnd) {
			ch = m_data[iii];
		} else {
			ch = 0;
		}
		col++;
		if (ch=='\n') {
			line++;
			col = 0;
		}
	}
	char buf[256];
	snprintf(buf, 256, "(line: %d, col: %d)", line, col);
	return buf;
}
