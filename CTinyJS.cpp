/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "CTinyJS.h"
#include "CScriptException.h"
#include "CScriptVarLink.h"
#include "debug.h"

CTinyJS::CTinyJS() {
	m_l = nullptr;
	m_root = (new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
	// Add built-in classes
	m_stringClass = (new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
	m_arrayClass = (new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
	m_objectClass = (new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT))->ref();
	m_root->addChild("String", m_stringClass);
	m_root->addChild("Array", m_arrayClass);
	m_root->addChild("Object", m_objectClass);
}

CTinyJS::~CTinyJS() {
	assert(m_l == nullptr);
	m_scopes.clear();
	m_stringClass->unref();
	m_arrayClass->unref();
	m_objectClass->unref();
	m_root->unref();
#ifdef DEBUG_MEMORY
	show_allocated();
#endif
}

void CTinyJS::trace() {
	m_root->trace();
}

void CTinyJS::execute(const std::string& _code) {
	CScriptLex* oldLex = m_l;
	std::vector<CScriptVar*> oldScopes = m_scopes;
	m_l = new CScriptLex(_code);
#ifdef TINYJS_CALL_STACK
	PARSERJS_VERBOSE("Clear current stack history");
	m_call_stack.clear();
#endif
	PARSERJS_VERBOSE("Clear scopes");
	m_scopes.clear();
	m_scopes.push_back(m_root);
	try {
		bool execute = true;
		while (m_l->m_tk) {
			statement(execute);
		}
	} catch (CScriptException *e) {
		std::ostringstream msg;
		msg << "Error " << e->what();
#ifdef TINYJS_CALL_STACK
		for (int32_t iii=int32_t(m_call_stack.size())-1; iii>=0; --iii) {
			msg << "\n" << iii << ": " << m_call_stack.at(iii);
		}
#endif
		msg << " at " << m_l->getPosition();
		delete m_l;
		m_l = oldLex;
		throw new CScriptException(msg.str());
	}
	delete m_l;
	m_l = oldLex;
	m_scopes = oldScopes;
}

CScriptVarLink CTinyJS::evaluateComplex(const std::string& _code) {
	CScriptLex *oldLex = m_l;
	std::vector<CScriptVar*> oldScopes = m_scopes;
	m_l = new CScriptLex(_code);
#ifdef TINYJS_CALL_STACK
	m_call_stack.clear();
#endif
	m_scopes.clear();
	m_scopes.push_back(m_root);
	CScriptVarLink *v = nullptr;
	try {
		bool execute = true;
		do {
			CLEAN(v);
			v = base(execute);
			if (m_l->m_tk!=LEX_EOF) {
				m_l->match(';');
			}
		} while (m_l->m_tk!=LEX_EOF);
	} catch (CScriptException *e) {
		std::ostringstream msg;
		msg << "Error " << e->what();
#ifdef TINYJS_CALL_STACK
		for (int32_t iii=int32_t(m_call_stack.size())-1; iii>=0; --iii) {
			msg << "\n" << iii << ": " << m_call_stack.at(iii);
		}
#endif
		msg << " at " << m_l->getPosition();
		delete m_l;
		m_l = oldLex;
		throw new CScriptException(msg.str());
	}
	delete m_l;
	m_l = oldLex;
	m_scopes = oldScopes;
	if (v) {
		CScriptVarLink r = *v;
		CLEAN(v);
		return r;
	}
	// return undefined...
	return CScriptVarLink(new CScriptVar());
}

std::string CTinyJS::evaluate(const std::string& _code) {
	return evaluateComplex(_code).m_var->getString();
}

void CTinyJS::parseFunctionArguments(CScriptVar* _funcVar) {
	m_l->match('(');
	while (m_l->m_tk!=')') {
		_funcVar->addChildNoDup(m_l->m_tkStr);
		m_l->match(LEX_ID);
		if (m_l->m_tk!=')') {
			m_l->match(',');
		}
	}
	m_l->match(')');
}

void CTinyJS::addNative(const std::string& _funcDesc, JSCallback _ptr, void* _userdata) {
	CScriptLex *oldLex = m_l;
	m_l = new CScriptLex(_funcDesc);
	CScriptVar *base = m_root;
	m_l->match(LEX_R_FUNCTION);
	std::string funcName = m_l->m_tkStr;
	m_l->match(LEX_ID);
	/* Check for dots, we might want to do something like function std::string.substring ... */
	while (m_l->m_tk == '.') {
		m_l->match('.');
		CScriptVarLink *link = base->findChild(funcName);
		// if it doesn't exist, make an object class
		if (!link) {
			link = base->addChild(funcName, new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT));
		}
		base = link->m_var;
		funcName = m_l->m_tkStr;
		m_l->match(LEX_ID);
	}
	CScriptVar *funcVar = new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION | SCRIPTVAR_NATIVE);
	funcVar->setCallback(_ptr, _userdata);
	parseFunctionArguments(funcVar);
	delete m_l;
	m_l = oldLex;
	base->addChild(funcName, funcVar);
}

CScriptVarLink *CTinyJS::parseFunctionDefinition() {
	// actually parse a function...
	m_l->match(LEX_R_FUNCTION);
	std::string funcName = TINYJS_TEMP_NAME;
	/* we can have functions without names */
	if (m_l->m_tk==LEX_ID) {
		funcName = m_l->m_tkStr;
		m_l->match(LEX_ID);
	}
	CScriptVarLink *funcVar = new CScriptVarLink(new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION), funcName);
	parseFunctionArguments(funcVar->m_var);
	int funcBegin = m_l->m_tokenStart;
	bool noexecute = false;
	block(noexecute);
	funcVar->m_var->m_data = m_l->getSubString(funcBegin);
	return funcVar;
}

/** Handle a function call (assumes we've parsed the function name and we're
 * on the start bracket). 'parent' is the object that contains this method,
 * if there was one (otherwise it's just a normnal function).
 */
CScriptVarLink* CTinyJS::functionCall(bool& _execute, CScriptVarLink* _function, CScriptVar* _parent) {
	PARSERJS_VERBOSE("    functionCall()");
	if (_execute) {
		if (!_function->m_var->isFunction()) {
			std::string errorMsg = "Expecting '";
			errorMsg = errorMsg + _function->m_name + "' to be a function";
			throw new CScriptException(errorMsg.c_str());
		}
		m_l->match('(');
		// create a new symbol table entry for execution of this function
		CScriptVar *functionRoot = new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_FUNCTION);
		if (_parent) {
			functionRoot->addChildNoDup("this", _parent);
		}
		// grab in all parameters
		CScriptVarLink *v = _function->m_var->m_firstChild;
		while (v) {
			CScriptVarLink *value = base(_execute);
			if (_execute) {
				if (value->m_var->isBasic()) {
					// pass by value
					functionRoot->addChild(v->m_name, value->m_var->deepCopy());
				} else {
					// pass by reference
					functionRoot->addChild(v->m_name, value->m_var);
				}
			}
			CLEAN(value);
			if (m_l->m_tk!=')') {
				m_l->match(',');
			}
			v = v->m_nextSibling;
		}
		m_l->match(')');
		// setup a return variable
		CScriptVarLink *returnVar = nullptr;
		// execute function!
		// add the function's execute space to the symbol table so we can recurse
		CScriptVarLink* returnVarLink = functionRoot->addChild(TINYJS_RETURN_VAR);
		m_scopes.push_back(functionRoot);
#ifdef TINYJS_CALL_STACK
		m_call_stack.push_back(_function->m_name + " from " + m_l->getPosition());
#endif
		if (_function->m_var->isNative()) {
			assert(_function->m_var->m_jsCallback);
			_function->m_var->m_jsCallback(functionRoot, _function->m_var->m_jsCallbackUserData);
		} else {
			/* we just want to execute the block, but something could
			 * have messed up and left us with the wrong ScriptLex, so
			 * we want to be careful here... */
			CScriptException* exception = nullptr;
			CScriptLex *oldLex = m_l;
			CScriptLex *newLex = new CScriptLex(_function->m_var->getString());
			m_l = newLex;
			try {
				block(_execute);
				// because return will probably have called this, and set execute to false
				_execute = true;
			} catch (CScriptException *e) {
				exception = e;
			}
			delete newLex;
			m_l = oldLex;
			if (exception != nullptr) {
				throw exception;
			}
		}
#ifdef TINYJS_CALL_STACK
		if (!m_call_stack.empty()) {
			m_call_stack.pop_back();
		}
#endif
		m_scopes.pop_back();
		/* get the real return var before we remove it from our function */
		returnVar = new CScriptVarLink(returnVarLink->m_var);
		functionRoot->removeLink(returnVarLink);
		delete functionRoot;
		if (returnVar) {
			return returnVar;
		} else {
			return new CScriptVarLink(new CScriptVar());
		}
	} else {
		// function, but not executing - just parse args and be done
		m_l->match('(');
		while (m_l->m_tk != ')') {
			CScriptVarLink *value = base(_execute);
			CLEAN(value);
			if (m_l->m_tk!=')') {
				m_l->match(',');
			}
		}
		m_l->match(')');
		if (m_l->m_tk == '{') {
			// TODO: why is this here?
			block(_execute);
		}
		/* function will be a blank scriptvarlink if we're not executing,
		 * so just return it rather than an alloc/free */
		return _function;
	}
}

CScriptVarLink* CTinyJS::factor(bool& _execute) {
	PARSERJS_VERBOSE("    factor()");
	if (m_l->m_tk=='(') {
		m_l->match('(');
		CScriptVarLink *a = base(_execute);
		m_l->match(')');
		return a;
	}
	if (m_l->m_tk == LEX_R_TRUE) {
		m_l->match(LEX_R_TRUE);
		return new CScriptVarLink(new CScriptVar(1));
	}
	if (m_l->m_tk == LEX_R_FALSE) {
		m_l->match(LEX_R_FALSE);
		return new CScriptVarLink(new CScriptVar(0));
	}
	if (m_l->m_tk == LEX_R_NULL) {
		m_l->match(LEX_R_NULL);
		return new CScriptVarLink(new CScriptVar(TINYJS_BLANK_DATA,SCRIPTVAR_NULL));
	}
	if (m_l->m_tk == LEX_R_UNDEFINED) {
		m_l->match(LEX_R_UNDEFINED);
		return new CScriptVarLink(new CScriptVar(TINYJS_BLANK_DATA,SCRIPTVAR_UNDEFINED));
	}
	if (m_l->m_tk == LEX_ID) {
		CScriptVarLink *a = nullptr;
		if (_execute == true) {
			a = findInScopes(m_l->m_tkStr);
		} else {
			a = new CScriptVarLink(new CScriptVar());
		}
		//printf("0x%08X for %s at %s\n", (unsigned int)a, l->tkStr.c_str(), l->getPosition().c_str());
		/* The parent if we're executing a method call */
		CScriptVar *parent = 0;
		if (_execute && !a) {
			/* Variable doesn't exist! JavaScript says we should create it
			 * (we won't add it here. This is done in the assignment operator)*/
			a = new CScriptVarLink(new CScriptVar(), m_l->m_tkStr);
		}
		m_l->match(LEX_ID);
		while (    m_l->m_tk=='('
		        || m_l->m_tk=='.'
		        || m_l->m_tk=='[') {
			if (m_l->m_tk=='(') {
				// ------------------------------------- Function Call
				a = functionCall(_execute, a, parent);
			} else if (m_l->m_tk == '.') {
				// ------------------------------------- Record Access
				m_l->match('.');
				if (_execute) {
					const std::string &name = m_l->m_tkStr;
					CScriptVarLink *child = a->m_var->findChild(name);
					if (child == nullptr) {
						child = findInParentClasses(a->m_var, name);
					}
					if (child == nullptr) {
						/* if we haven't found this defined yet, use the built-in 'length' properly */
						if (    a->m_var->isArray() == true
						     && name == "length") {
							int32_t val = a->m_var->getArrayLength();
							child = new CScriptVarLink(new CScriptVar(val));
						} else if (a->m_var->isString() && name == "length") {
							int32_t val = a->m_var->getString().size();
							child = new CScriptVarLink(new CScriptVar(val));
						} else {
							child = a->m_var->addChild(name);
						}
					}
					parent = a->m_var;
					a = child;
				}
				m_l->match(LEX_ID);
			} else if (m_l->m_tk == '[') {
				// ------------------------------------- Array Access
				m_l->match('[');
				CScriptVarLink *index = base(_execute);
				m_l->match(']');
				if (_execute) {
					CScriptVarLink *child = a->m_var->findChildOrCreate(index->m_var->getString());
					parent = a->m_var;
					a = child;
				}
				CLEAN(index);
			} else assert(0);
		}
		return a;
	}
	if (    m_l->m_tk == LEX_INT
	     || m_l->m_tk == LEX_FLOAT) {
		CScriptVar *a = new CScriptVar(m_l->m_tkStr, ((m_l->m_tk==LEX_INT)?SCRIPTVAR_INTEGER:SCRIPTVAR_DOUBLE));
		m_l->match(m_l->m_tk);
		return new CScriptVarLink(a);
	}
	if (m_l->m_tk == LEX_STR) {
		CScriptVar *a = new CScriptVar(m_l->m_tkStr, SCRIPTVAR_STRING);
		m_l->match(LEX_STR);
		return new CScriptVarLink(a);
	}
	if (m_l->m_tk == '{') {
		CScriptVar *contents = new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT);
		/* JSON-style object definition */
		m_l->match('{');
		while (m_l->m_tk != '}') {
			std::string id = m_l->m_tkStr;
			// we only allow std::strings or IDs on the left hand side of an initialisation
			if (m_l->m_tk==LEX_STR) {
				m_l->match(LEX_STR);
			} else {
				m_l->match(LEX_ID);
			}
			m_l->match(':');
			if (_execute) {
				CScriptVarLink *a = base(_execute);
				contents->addChild(id, a->m_var);
				CLEAN(a);
			}
			// no need to clean here, as it will definitely be used
			if (m_l->m_tk != '}') {
				m_l->match(',');
			}
		}
		m_l->match('}');
		return new CScriptVarLink(contents);
	}
	if (m_l->m_tk=='[') {
		CScriptVar *contents = new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_ARRAY);
		/* JSON-style array */
		m_l->match('[');
		int idx = 0;
		while (m_l->m_tk != ']') {
			if (_execute == true) {
				char idx_str[16]; // big enough for 2^32
				snprintf(idx_str, sizeof(idx_str), "%d", idx);
				CScriptVarLink *a = base(_execute);
				contents->addChild(idx_str, a->m_var);
				CLEAN(a);
			}
			// no need to clean here, as it will definitely be used
			if (m_l->m_tk != ']') {
				m_l->match(',');
			}
			idx++;
		}
		m_l->match(']');
		return new CScriptVarLink(contents);
	}
	if (m_l->m_tk == LEX_R_FUNCTION) {
		CScriptVarLink *funcVar = parseFunctionDefinition();
		if (funcVar->m_name != TINYJS_TEMP_NAME) {
			printf("Functions not defined at statement-level are not meant to have a name");
		}
		return funcVar;
	}
	if (m_l->m_tk == LEX_R_NEW) {
		// new -> create a new object
		m_l->match(LEX_R_NEW);
		const std::string &className = m_l->m_tkStr;
		if (_execute == true) {
			CScriptVarLink *objClassOrFunc = findInScopes(className);
			if (!objClassOrFunc) {
				printf("%s is not a valid class name", className.c_str());
				return new CScriptVarLink(new CScriptVar());
			}
			m_l->match(LEX_ID);
			CScriptVar* obj = new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_OBJECT);
			CScriptVarLink* objLink = new CScriptVarLink(obj);
			if (objClassOrFunc->m_var->isFunction()) {
				CLEAN(functionCall(_execute, objClassOrFunc, obj));
			} else {
				obj->addChild(TINYJS_PROTOTYPE_CLASS, objClassOrFunc->m_var);
				if (m_l->m_tk == '(') {
					m_l->match('(');
					m_l->match(')');
				}
			}
			return objLink;
		} else {
			m_l->match(LEX_ID);
			if (m_l->m_tk == '(') {
				m_l->match('(');
				m_l->match(')');
			}
		}
	}
	// Nothing we can do here... just hope it's the end...
	m_l->match(LEX_EOF);
	return 0;
}

CScriptVarLink* CTinyJS::unary(bool& _execute) {
	PARSERJS_VERBOSE("    unary()");
	CScriptVarLink *a;
	if (m_l->m_tk=='!') {
		m_l->match('!'); // binary not
		a = factor(_execute);
		if (_execute) {
			CScriptVar zero(0);
			CScriptVar *res = a->m_var->mathsOp(&zero, LEX_EQUAL);
			CREATE_LINK(a, res);
		}
	} else {
		a = factor(_execute);
	}
	return a;
}

CScriptVarLink *CTinyJS::term(bool& _execute) {
	PARSERJS_VERBOSE("    term()");
	CScriptVarLink *a = unary(_execute);
	while (    m_l->m_tk=='*'
	        || m_l->m_tk=='/'
	        || m_l->m_tk=='%') {
		int op = m_l->m_tk;
		m_l->match(m_l->m_tk);
		CScriptVarLink *b = unary(_execute);
		if (_execute == true) {
			CScriptVar *res = a->m_var->mathsOp(b->m_var, op);
			CREATE_LINK(a, res);
		}
		CLEAN(b);
	}
	return a;
}

CScriptVarLink *CTinyJS::expression(bool& _execute) {
	PARSERJS_VERBOSE("    expression()");
	bool negate = false;
	if (m_l->m_tk == '-') {
		m_l->match('-');
		negate = true;
	}
	CScriptVarLink *a = term(_execute);
	if (negate) {
		CScriptVar zero(0);
		CScriptVar *res = zero.mathsOp(a->m_var, '-');
		CREATE_LINK(a, res);
	}
	while (    m_l->m_tk=='+'
	        || m_l->m_tk=='-'
	        || m_l->m_tk==LEX_PLUSPLUS
	        || m_l->m_tk==LEX_MINUSMINUS) {
		int op = m_l->m_tk;
		m_l->match(m_l->m_tk);
		if (    op==LEX_PLUSPLUS
		     || op==LEX_MINUSMINUS) {
			if (_execute) {
				CScriptVar one(1);
				CScriptVar *res = a->m_var->mathsOp(&one, op == LEX_PLUSPLUS ? '+' : '-');
				CScriptVarLink *oldValue = new CScriptVarLink(a->m_var);
				// in-place add/subtract
				a->replaceWith(res);
				CLEAN(a);
				a = oldValue;
			}
		} else {
			CScriptVarLink *b = term(_execute);
			if (_execute) {
				// not in-place, so just replace
				CScriptVar *res = a->m_var->mathsOp(b->m_var, op);
				CREATE_LINK(a, res);
			}
			CLEAN(b);
		}
	}
	return a;
}

CScriptVarLink* CTinyJS::shift(bool& _execute) {
	PARSERJS_VERBOSE("    shift()");
	CScriptVarLink *a = expression(_execute);
	if (    m_l->m_tk==LEX_LSHIFT
	     || m_l->m_tk==LEX_RSHIFT
	     || m_l->m_tk==LEX_RSHIFTUNSIGNED) {
		int op = m_l->m_tk;
		m_l->match(op);
		CScriptVarLink *b = base(_execute);
		int shift = _execute ? b->m_var->getInt() : 0;
		CLEAN(b);
		if (_execute) {
			if (op==LEX_LSHIFT) {
				a->m_var->setInt(a->m_var->getInt() << shift);
			}
			if (op==LEX_RSHIFT) {
				a->m_var->setInt(a->m_var->getInt() >> shift);
			}
			if (op==LEX_RSHIFTUNSIGNED) {
				a->m_var->setInt(((unsigned int)a->m_var->getInt()) >> shift);
			}
		}
	}
	return a;
}

CScriptVarLink *CTinyJS::condition(bool &_execute) {
	PARSERJS_VERBOSE("    condition()");
	CScriptVarLink *a = shift(_execute);
	CScriptVarLink *b;
	while (    m_l->m_tk==LEX_EQUAL
	        || m_l->m_tk==LEX_NEQUAL
	        || m_l->m_tk==LEX_TYPEEQUAL
	        || m_l->m_tk==LEX_NTYPEEQUAL
	        || m_l->m_tk==LEX_LEQUAL
	        || m_l->m_tk==LEX_GEQUAL
	        || m_l->m_tk=='<'
	        || m_l->m_tk=='>') {
		int op = m_l->m_tk;
		m_l->match(m_l->m_tk);
		b = shift(_execute);
		if (_execute) {
			CScriptVar *res = a->m_var->mathsOp(b->m_var, op);
			CREATE_LINK(a, res);
		}
		CLEAN(b);
	}
	return a;
}

CScriptVarLink *CTinyJS::logic(bool &_execute) {
	PARSERJS_VERBOSE("    logic()");
	CScriptVarLink *a = condition(_execute);
	CScriptVarLink *b;
	while (    m_l->m_tk=='&'
	        || m_l->m_tk=='|'
	        || m_l->m_tk=='^'
	        || m_l->m_tk==LEX_ANDAND
	        || m_l->m_tk==LEX_OROR) {
		bool noexecute = false;
		int op = m_l->m_tk;
		m_l->match(m_l->m_tk);
		bool shortCircuit = false;
		bool boolean = false;
		// if we have short-circuit ops, then if we know the outcome
		// we don't bother to execute the other op. Even if not
		// we need to tell mathsOp it's an & or |
		if (op==LEX_ANDAND) {
			op = '&';
			shortCircuit = !a->m_var->getBool();
			boolean = true;
		} else if (op==LEX_OROR) {
			op = '|';
			shortCircuit = a->m_var->getBool();
			boolean = true;
		}
		b = condition(shortCircuit ? noexecute : _execute);
		if (_execute && !shortCircuit) {
			if (boolean) {
				CScriptVar *newa = new CScriptVar(a->m_var->getBool());
				CScriptVar *newb = new CScriptVar(b->m_var->getBool());
				CREATE_LINK(a, newa);
				CREATE_LINK(b, newb);
			}
			CScriptVar *res = a->m_var->mathsOp(b->m_var, op);
			CREATE_LINK(a, res);
		}
		CLEAN(b);
	}
	return a;
}

CScriptVarLink *CTinyJS::ternary(bool &_execute) {
	PARSERJS_VERBOSE("    ternary");
	CScriptVarLink *lhs = logic(_execute);
	bool noexec = false;
	if (m_l->m_tk=='?') {
		m_l->match('?');
		if (!_execute) {
			CLEAN(lhs);
			CLEAN(base(noexec));
			m_l->match(':');
			CLEAN(base(noexec));
		} else {
			bool first = lhs->m_var->getBool();
			CLEAN(lhs);
			if (first) {
				lhs = base(_execute);
				m_l->match(':');
				CLEAN(base(noexec));
			} else {
				CLEAN(base(noexec));
				m_l->match(':');
				lhs = base(_execute);
			}
		}
	}
	return lhs;
}

CScriptVarLink *CTinyJS::base(bool &_execute) {
	PARSERJS_VERBOSE("    base()");
	CScriptVarLink *lhs = ternary(_execute);
	if (    m_l->m_tk=='='
	     || m_l->m_tk==LEX_PLUSEQUAL
	     || m_l->m_tk==LEX_MINUSEQUAL) {
		/* If we're assigning to this and we don't have a parent,
		 * add it to the symbol table root as per JavaScript. */
		if (_execute && !lhs->m_owned) {
			if (lhs->m_name.length()>0) {
				CScriptVarLink *realLhs = m_root->addChildNoDup(lhs->m_name, lhs->m_var);
				CLEAN(lhs);
				lhs = realLhs;
			} else {
				printf("Trying to assign to an un-named type\n");
			}
		}
		int32_t op = m_l->m_tk;
		m_l->match(m_l->m_tk);
		CScriptVarLink *rhs = base(_execute);
		if (_execute) {
			if (op=='=') {
				lhs->replaceWith(rhs);
			} else if (op == LEX_PLUSEQUAL) {
				CScriptVar *res = lhs->m_var->mathsOp(rhs->m_var, '+');
				lhs->replaceWith(res);
			} else if (op == LEX_MINUSEQUAL) {
				CScriptVar *res = lhs->m_var->mathsOp(rhs->m_var, '-');
				lhs->replaceWith(res);
			} else {
				assert(0);
			}
		}
		CLEAN(rhs);
	}
	return lhs;
}

void CTinyJS::block(bool& _execute) {
	PARSERJS_VERBOSE("    block()");
	m_l->match('{');
	if (_execute) {
		while (    m_l->m_tk
		        && m_l->m_tk!='}') {
			statement(_execute);
		}
		m_l->match('}');
	} else {
		// fast skip of blocks
		int brackets = 1;
		while (    m_l->m_tk
		        && brackets) {
			if (m_l->m_tk == '{') {
				brackets++;
			}
			if (m_l->m_tk == '}') {
				brackets--;
			}
			m_l->match(m_l->m_tk);
		}
	}
}

void CTinyJS::statement(bool &_execute) {
	PARSERJS_VERBOSE("statement : " << m_l->getTokenStr(m_l->m_tk));
	if (    m_l->m_tk == LEX_ID
	     || m_l->m_tk == LEX_INT
	     || m_l->m_tk == LEX_FLOAT
	     || m_l->m_tk == LEX_STR
	     || m_l->m_tk == '-') {
		/* Execute a simple statement that only contains basic arithmetic... */
		CLEAN(base(_execute));
		m_l->match(';');
	} else if (m_l->m_tk=='{') {
		/* A block of code */
		block(_execute);
	} else if (m_l->m_tk==';') {
		/* Empty statement - to allow things like ;;; */
		m_l->match(';');
	} else if (m_l->m_tk==LEX_R_VAR) {
		/* variable creation. TODO - we need a better way of parsing the left
		 * hand side. Maybe just have a flag called can_create_var that we
		 * set and then we parse as if we're doing a normal equals.*/
		m_l->match(LEX_R_VAR);
		while (m_l->m_tk != ';') {
			CScriptVarLink *a = 0;
			if (_execute) {
				a = m_scopes.back()->findChildOrCreate(m_l->m_tkStr);
			}
			m_l->match(LEX_ID);
			// now do stuff defined with dots
			while (m_l->m_tk == '.') {
				m_l->match('.');
				if (_execute) {
					CScriptVarLink *lastA = a;
					a = lastA->m_var->findChildOrCreate(m_l->m_tkStr);
				}
				m_l->match(LEX_ID);
			}
			// sort out initialiser
			if (m_l->m_tk == '=') {
				m_l->match('=');
				CScriptVarLink *var = base(_execute);
				if (_execute) {
					a->replaceWith(var);
				}
				CLEAN(var);
			}
			if (m_l->m_tk != ';') {
				m_l->match(',');
			}
		}
		m_l->match(';');
	} else if (m_l->m_tk==LEX_R_IF) {
		m_l->match(LEX_R_IF);
		m_l->match('(');
		CScriptVarLink *var = base(_execute);
		m_l->match(')');
		bool cond = _execute && var->m_var->getBool();
		CLEAN(var);
		bool noexecute = false; // because we need to be abl;e to write to it
		statement(cond ? _execute : noexecute);
		if (m_l->m_tk == LEX_R_ELSE) {
			m_l->match(LEX_R_ELSE);
			statement(cond ? noexecute : _execute);
		}
	} else if (m_l->m_tk==LEX_R_WHILE) {
		// We do repetition by pulling out the std::string representing our statement
		// there's definitely some opportunity for optimisation here
		m_l->match(LEX_R_WHILE);
		m_l->match('(');
		int32_t whileCondStart = m_l->m_tokenStart;
		bool noexecute = false;
		CScriptVarLink* cond = base(_execute);
		bool loopCond = _execute && cond->m_var->getBool();
		CLEAN(cond);
		CScriptLex* whileCond = m_l->getSubLex(whileCondStart);
		m_l->match(')');
		int whileBodyStart = m_l->m_tokenStart;
		statement(loopCond ? _execute : noexecute);
		CScriptLex *whileBody = m_l->getSubLex(whileBodyStart);
		CScriptLex *oldLex = m_l;
		int loopCount = TINYJS_LOOP_MAX_ITERATIONS;
		while (    loopCond
		        && loopCount-->0) {
			whileCond->reset();
			m_l = whileCond;
			cond = base(_execute);
			loopCond = _execute && cond->m_var->getBool();
			CLEAN(cond);
			if (loopCond) {
				whileBody->reset();
				m_l = whileBody;
				statement(_execute);
			}
		}
		m_l = oldLex;
		delete whileCond;
		delete whileBody;
		if (loopCount<=0) {
			m_root->trace();
			printf("WHILE Loop exceeded %d iterations at %s\n", TINYJS_LOOP_MAX_ITERATIONS, m_l->getPosition().c_str());
			throw new CScriptException("LOOP_ERROR");
		}
	} else if (m_l->m_tk==LEX_R_FOR) {
		m_l->match(LEX_R_FOR);
		m_l->match('(');
		statement(_execute); // initialisation
		//l->match(';');
		int forCondStart = m_l->m_tokenStart;
		bool noexecute = false;
		CScriptVarLink *cond = base(_execute); // condition
		bool loopCond = _execute && cond->m_var->getBool();
		CLEAN(cond);
		CScriptLex *forCond = m_l->getSubLex(forCondStart);
		m_l->match(';');
		int forIterStart = m_l->m_tokenStart;
		CLEAN(base(noexecute)); // iterator
		CScriptLex *forIter = m_l->getSubLex(forIterStart);
		m_l->match(')');
		int forBodyStart = m_l->m_tokenStart;
		statement(loopCond ? _execute : noexecute);
		CScriptLex *forBody = m_l->getSubLex(forBodyStart);
		CScriptLex *oldLex = m_l;
		if (loopCond) {
			forIter->reset();
			m_l = forIter;
			CLEAN(base(_execute));
		}
		int loopCount = TINYJS_LOOP_MAX_ITERATIONS;
		while (_execute && loopCond && loopCount-->0) {
			forCond->reset();
			m_l = forCond;
			cond = base(_execute);
			loopCond = cond->m_var->getBool();
			CLEAN(cond);
			if (_execute && loopCond) {
				forBody->reset();
				m_l = forBody;
				statement(_execute);
			}
			if (_execute && loopCond) {
				forIter->reset();
				m_l = forIter;
				CLEAN(base(_execute));
			}
		}
		m_l = oldLex;
		delete forCond;
		delete forIter;
		delete forBody;
		if (loopCount<=0) {
			m_root->trace();
			printf("FOR Loop exceeded %d iterations at %s\n", TINYJS_LOOP_MAX_ITERATIONS, m_l->getPosition().c_str());
			throw new CScriptException("LOOP_ERROR");
		}
	} else if (m_l->m_tk==LEX_R_RETURN) {
		m_l->match(LEX_R_RETURN);
		CScriptVarLink* result = 0;
		if (m_l->m_tk != ';') {
			result = base(_execute);
		}
		if (_execute) {
			CScriptVarLink *resultVar = m_scopes.back()->findChild(TINYJS_RETURN_VAR);
			if (resultVar) {
				resultVar->replaceWith(result);
			} else {
				printf("RETURN statement, but not in a function.\n");
			}
			_execute = false;
		}
		CLEAN(result);
		m_l->match(';');
	} else if (m_l->m_tk==LEX_R_FUNCTION) {
		CScriptVarLink *funcVar = parseFunctionDefinition();
		if (_execute) {
			if (funcVar->m_name == TINYJS_TEMP_NAME) {
				printf("Functions defined at statement-level are meant to have a name\n");
			} else {
				m_scopes.back()->addChildNoDup(funcVar->m_name, funcVar->m_var);
			}
		}
		CLEAN(funcVar);
	} else {
		m_l->match(LEX_EOF);
	}
}

/// Get the given variable specified by a path (var1.var2.etc), or return 0
CScriptVar* CTinyJS::getScriptVariable(const std::string& _path) {
	// traverse path
	size_t prevIdx = 0;
	size_t thisIdx = _path.find('.');
	if (thisIdx == std::string::npos) {
		thisIdx = _path.length();
	}
	CScriptVar *var = m_root;
	while (    var
	        && prevIdx < _path.length()) {
		std::string el = _path.substr(prevIdx, thisIdx-prevIdx);
		CScriptVarLink *varl = var->findChild(el);
		var = varl ? varl->m_var : 0;
		prevIdx = thisIdx+1;
		thisIdx = _path.find('.', prevIdx);
		if (thisIdx == std::string::npos) {
			thisIdx = _path.length();
		}
	}
	return var;
}

/// Get the value of the given variable, or return 0
const std::string *CTinyJS::getVariable(const std::string& _path) {
	CScriptVar *var = getScriptVariable(_path);
	// return result
	if (var) {
		return &var->getString();
	} else {
		return 0;
	}
}

/// set the value of the given variable, return trur if it exists and gets set
bool CTinyJS::setVariable(const std::string& _path, const std::string& _varData) {
	CScriptVar *var = getScriptVariable(_path);
	// return result
	if (var) {
		if (var->isInt()) {
			var->setInt((int)strtol(_varData.c_str(),0,0));
		} else if (var->isDouble()) {
			var->setDouble(strtod(_varData.c_str(),0));
		} else {
			var->setString(_varData.c_str());
		}
		return true;
	} else {
		return false;
	}
}

/// Finds a child, looking recursively up the scopes
CScriptVarLink *CTinyJS::findInScopes(const std::string& _childName) {
	for (int32_t sss=m_scopes.size()-1; sss>=0; --sss) {
		CScriptVarLink *v = m_scopes[sss]->findChild(_childName);
		if (v) {
			return v;
		}
	}
	return nullptr;

}

/// Look up in any parent classes of the given object
CScriptVarLink* CTinyJS::findInParentClasses(CScriptVar* _object, const std::string& _name) {
	// Look for links to actual parent classes
	CScriptVarLink* parentClass = _object->findChild(TINYJS_PROTOTYPE_CLASS);
	while (parentClass != nullptr) {
		CScriptVarLink *implementation = parentClass->m_var->findChild(_name);
		if (implementation) {
			return implementation;
		}
		parentClass = parentClass->m_var->findChild(TINYJS_PROTOTYPE_CLASS);
	}
	// else fake it for std::strings and finally objects
	if (_object->isString()) {
		CScriptVarLink *implementation = m_stringClass->findChild(_name);
		if (implementation) {
			return implementation;
		}
	}
	if (_object->isArray()) {
		CScriptVarLink *implementation = m_arrayClass->findChild(_name);
		if (implementation) {
			return implementation;
		}
	}
	CScriptVarLink *implementation = m_objectClass->findChild(_name);
	if (implementation) {
		return implementation;
	}
	return 0;
}
