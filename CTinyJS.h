/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_C_TiNY_JS_H__
#define __PARSERJS_C_TiNY_JS_H__

#include "tools.h"
#include "CScriptVar.h"
#include "CScriptLex.h"

class CTinyJS {
	public:
		CTinyJS();
		~CTinyJS();
		void execute(const std::string& _code);
		/** Evaluate the given code and return a link to a javascript object,
		 * useful for (dangerous) JSON parsing. If nothing to return, will return
		 * 'undefined' variable type. CScriptVarLink is returned as this will
		 * automatically unref the result as it goes out of scope. If you want to
		 * keep it, you must use ref() and unref() */
		CScriptVarLink evaluateComplex(const std::string& _code);
		/** Evaluate the given code and return a std::string. If nothing to return, will return
		 * 'undefined' */
		std::string evaluate(const std::string& _code);
		/// add a native function to be called from TinyJS
		/** example:
		   \code
			   void scRandInt(CScriptVar *c, void *userdata) { ... }
			   tinyJS->addNative("function randInt(min, max)", scRandInt, 0);
		   \endcode
		   or
		   \code
			   void scSubstring(CScriptVar *c, void *userdata) { ... }
			   tinyJS->addNative("function std::string.substring(lo, hi)", scSubstring, 0);
		   \endcode
		*/
		void addNative(const std::string& _funcDesc, JSCallback _ptr, void* _userdata);
		/// Get the given variable specified by a path (var1.var2.etc), or return 0
		CScriptVar *getScriptVariable(const std::string& _path);
		/// Get the value of the given variable, or return 0
		const std::string *getVariable(const std::string& _path);
		/// set the value of the given variable, return trur if it exists and gets set
		bool setVariable(const std::string& _path, const std::string& _varData);
		/// Send all variables to stdout
		void trace();
		CScriptVar *m_root;   /// root of symbol table
	private:
		CScriptLex *m_l;/// current lexer
		std::vector<CScriptVar*> m_scopes; /// stack of scopes when parsing
	#ifdef TINYJS_CALL_STACK
		std::vector<std::string> m_call_stack; /// Names of places called so we can show when erroring
	#endif
		CScriptVar *m_stringClass; /// Built in std::string class
		CScriptVar *m_objectClass; /// Built in object class
		CScriptVar *m_arrayClass; /// Built in array class
		// parsing - in order of precedence
		CScriptVarLink *functionCall(bool& _execute, CScriptVarLink* _function, CScriptVar* _parent);
		CScriptVarLink *factor(bool& _execute);
		CScriptVarLink *unary(bool& _execute);
		CScriptVarLink *term(bool& _execute);
		CScriptVarLink *expression(bool& _execute);
		CScriptVarLink *shift(bool& _execute);
		CScriptVarLink *condition(bool& _execute);
		CScriptVarLink *logic(bool& _execute);
		CScriptVarLink *ternary(bool& _execute);
		CScriptVarLink *base(bool& _execute);
		void block(bool& _execute);
		void statement(bool& _execute);
		// parsing utility functions
		CScriptVarLink *parseFunctionDefinition();
		void parseFunctionArguments(CScriptVar* _funcVar);
		CScriptVarLink *findInScopes(const std::string& _childName); ///< Finds a child, looking recursively up the scopes
		/// Look up in any parent classes of the given object
		CScriptVarLink *findInParentClasses(CScriptVar* _object, const std::string& _name);
};

#endif
