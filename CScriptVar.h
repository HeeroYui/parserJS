/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_C_SCRIPT_VAR_H__
#define __PARSERJS_C_SCRIPT_VAR_H__

#include "tools.h"

class CScriptVarLink;
class CScriptVar;
typedef void (*JSCallback)(CScriptVar* _var, void* _userdata);
/// Variable class (containing a doubly-linked list of children)
class CScriptVar {
	public:
		CScriptVar(); ///< Create undefined
		CScriptVar(const std::string& _varData, int32_t _varFlags); ///< User defined
		CScriptVar(const std::string& _str); ///< Create a string
		CScriptVar(double _varData);
		CScriptVar(int32_t _val);
		~CScriptVar(void);
		CScriptVar *getReturnVar(); ///< If this is a function, get the result value (for use by native functions)
		void setReturnVar(CScriptVar* _var); ///< Set the result value. Use this when setting complex return data as it avoids a deepCopy()
		CScriptVar *getParameter(const std::string& _name); ///< If this is a function, get the parameter with the given name (for use by native functions)
		CScriptVarLink *findChild(const std::string& _childName); ///< Tries to find a child with the given name, may return 0
		CScriptVarLink *findChildOrCreate(const std::string& _childName, int32_t _varFlags=SCRIPTVAR_UNDEFINED); ///< Tries to find a child with the given name, or will create it with the given flags
		CScriptVarLink *findChildOrCreateByPath(const std::string& _path); ///< Tries to find a child with the given path (separated by dots)
		CScriptVarLink *addChild(const std::string& _childName, CScriptVar* _child=nullptr);
		CScriptVarLink *addChildNoDup(const std::string& _childName, CScriptVar* _child=nullptr); ///< add a child overwriting any with the same name
		void removeChild(CScriptVar* _child);
		void removeLink(CScriptVarLink* _link); ///< Remove a specific link (this is faster than finding via a child)
		void removeAllChildren();
		CScriptVar *getArrayIndex(int32_t _idx); ///< The the value at an array index
		void setArrayIndex(int _idx, CScriptVar* _value); ///< Set the value at an array index
		int getArrayLength(); ///< If this is an array, return the number of items in it (else 0)
		int getChildren(); ///< Get the number of children
		int getInt();
		bool getBool() {
			return getInt() != 0;
		}
		double getDouble();
		const std::string &getString();
		std::string getParsableString(); ///< get Data as a parsable javascript string
		void setInt(int _num);
		void setDouble(double _val);
		void setString(const std::string& _str);
		void setUndefined();
		void setArray();
		bool equals(CScriptVar* _v);
		bool isInt() { return (m_flags&SCRIPTVAR_INTEGER)!=0; }
		bool isDouble() { return (m_flags&SCRIPTVAR_DOUBLE)!=0; }
		bool isString() { return (m_flags&SCRIPTVAR_STRING)!=0; }
		bool isNumeric() { return (m_flags&SCRIPTVAR_NUMERICMASK)!=0; }
		bool isFunction() { return (m_flags&SCRIPTVAR_FUNCTION)!=0; }
		bool isObject() { return (m_flags&SCRIPTVAR_OBJECT)!=0; }
		bool isArray() { return (m_flags&SCRIPTVAR_ARRAY)!=0; }
		bool isNative() { return (m_flags&SCRIPTVAR_NATIVE)!=0; }
		bool isUndefined() { return (m_flags & SCRIPTVAR_VARTYPEMASK) == SCRIPTVAR_UNDEFINED; }
		bool isNull() { return (m_flags & SCRIPTVAR_NULL)!=0; }
		bool isBasic() { return m_firstChild == 0; } ///< Is this *not* an array/object/etc
		CScriptVar* mathsOp(CScriptVar* _b, int32_t _op); ///< do a maths op with another script variable
		void copyValue(CScriptVar* _val); ///< copy the value from the value given
		CScriptVar* deepCopy(); ///< deep copy this node and return the result
		void trace(std::string _indentStr = "", const std::string &_name = ""); ///< Dump out the contents of this using trace
		std::string getFlagsAsString(); ///< For debugging - just dump a string version of the flags
		void getJSON(std::ostringstream& _destination, const std::string _linePrefix=""); ///< Write out all the JS code needed to recreate this script variable to the stream (as JSON)
		void setCallback(JSCallback _callback, void* _userdata); ///< Set the callback for native functions
		CScriptVarLink* m_firstChild;
		CScriptVarLink* m_lastChild;
		/// For memory management/garbage collection
		CScriptVar* ref(); ///< Add reference to this variable
		void unref(); ///< Remove a reference, and delete this variable if required
		int getRefs(); ///< Get the number of references to this script variable
	protected:
		int m_refs; ///< The number of references held to this - used for garbage collection
		std::string m_data; ///< The contents of this variable if it is a string
		long m_intData; ///< The contents of this variable if it is an int
		double m_doubleData; ///< The contents of this variable if it is a double
		int m_flags; ///< the flags determine the type of the variable - int/double/string/etc
		JSCallback m_jsCallback; ///< Callback for native functions
		void* m_jsCallbackUserData; ///< user data passed as second argument to native functions
		void init(); ///< initialisation of data members
		/** Copy the basic data and flags from the variable given, with no
		  * children. Should be used internally only - by copyValue and deepCopy */
		void copySimpleData(CScriptVar* _val);
		friend class CTinyJS;
};

#endif
