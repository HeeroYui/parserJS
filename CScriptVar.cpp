/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "CScriptVar.h"
#include "CScriptVarLink.h"
#include "CScriptException.h"
#include "CScriptLex.h"
#include "debug.h"


CScriptVar::CScriptVar() {
	m_refs = 0;
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	init();
	m_flags = SCRIPTVAR_UNDEFINED;
}

CScriptVar::CScriptVar(const std::string& _str) {
	m_refs = 0;
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	init();
	m_flags = SCRIPTVAR_STRING;
	m_data = _str;
}


CScriptVar::CScriptVar(const std::string& _varData, int32_t _varFlags) {
	m_refs = 0;
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	init();
	m_flags = _varFlags;
	if (_varFlags & SCRIPTVAR_INTEGER) {
		m_intData = strtol(_varData.c_str(),0,0);
	} else if (_varFlags & SCRIPTVAR_DOUBLE) {
		m_doubleData = strtod(_varData.c_str(),0);
	} else {
		m_data = _varData;
	}
}

CScriptVar::CScriptVar(double _val) {
	m_refs = 0;
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	init();
	setDouble(_val);
}

CScriptVar::CScriptVar(int _val) {
	m_refs = 0;
#ifdef DEBUG_MEMORY
	mark_allocated(this);
#endif
	init();
	setInt(_val);
}

CScriptVar::~CScriptVar(void) {
#ifdef DEBUG_MEMORY
	mark_deallocated(this);
#endif
	removeAllChildren();
}

void CScriptVar::init() {
	m_firstChild = 0;
	m_lastChild = 0;
	m_flags = 0;
	m_jsCallback = 0;
	m_jsCallbackUserData = 0;
	m_data = TINYJS_BLANK_DATA;
	m_intData = 0;
	m_doubleData = 0;
}

CScriptVar* CScriptVar::getReturnVar() {
	return getParameter(TINYJS_RETURN_VAR);
}

void CScriptVar::setReturnVar(CScriptVar* _var) {
	findChildOrCreate(TINYJS_RETURN_VAR)->replaceWith(_var);
}

CScriptVar* CScriptVar::getParameter(const std::string& _name) {
	return findChildOrCreate(_name)->m_var;
}

CScriptVarLink* CScriptVar::findChild(const std::string& _childName) {
	CScriptVarLink *v = m_firstChild;
	while (v) {
		if (v->m_name.compare(_childName)==0) {
			return v;
		}
		v = v->m_nextSibling;
	}
	return 0;
}

CScriptVarLink* CScriptVar::findChildOrCreate(const std::string& _childName, int _varFlags) {
	CScriptVarLink* l = findChild(_childName);
	if (l) {
		return l;
	}
	return addChild(_childName, new CScriptVar(TINYJS_BLANK_DATA, _varFlags));
}

CScriptVarLink* CScriptVar::findChildOrCreateByPath(const std::string& _path) {
	size_t p = _path.find('.');
	if (p == std::string::npos) {
		return findChildOrCreate(_path);
	}
	return findChildOrCreate(_path.substr(0,p), SCRIPTVAR_OBJECT)->m_var->findChildOrCreateByPath(_path.substr(p+1));
}

CScriptVarLink* CScriptVar::addChild(const std::string& _childName, CScriptVar* _child) {
	if (isUndefined()) {
		m_flags = SCRIPTVAR_OBJECT;
	}
	// if no child supplied, create one
	if (!_child) {
		_child = new CScriptVar();
	}
	CScriptVarLink* link = new CScriptVarLink(_child, _childName);
	link->m_owned = true;
	if (m_lastChild) {
		m_lastChild->m_nextSibling = link;
		link->m_prevSibling = m_lastChild;
		m_lastChild = link;
	} else {
		m_firstChild = link;
		m_lastChild = link;
	}
	return link;
}

CScriptVarLink* CScriptVar::addChildNoDup(const std::string& _childName, CScriptVar* _child) {
	// if no child supplied, create one
	if (!_child) {
		_child = new CScriptVar();
	}
	CScriptVarLink *v = findChild(_childName);
	if (v) {
		v->replaceWith(_child);
	} else {
		v = addChild(_childName, _child);
	}
	return v;
}

void CScriptVar::removeChild(CScriptVar* _child) {
	CScriptVarLink* link = m_firstChild;
	while (link) {
		if (link->m_var == _child) {
			break;
		}
		link = link->m_nextSibling;
	}
	assert(link);
	removeLink(link);
}

void CScriptVar::removeLink(CScriptVarLink* _link) {
	if (!_link) {
		return;
	}
	if (_link->m_nextSibling) {
		_link->m_nextSibling->m_prevSibling = _link->m_prevSibling;
	}
	if (_link->m_prevSibling) {
		_link->m_prevSibling->m_nextSibling = _link->m_nextSibling;
	}
	if (m_lastChild == _link) {
		m_lastChild = _link->m_prevSibling;
	}
	if (m_firstChild == _link) {
		m_firstChild = _link->m_nextSibling;
	}
	delete _link;
}

void CScriptVar::removeAllChildren() {
	CScriptVarLink *c = m_firstChild;
	while (c) {
		CScriptVarLink *t = c->m_nextSibling;
		delete c;
		c = t;
	}
	m_firstChild = nullptr;
	m_lastChild = nullptr;
}

CScriptVar *CScriptVar::getArrayIndex(int32_t _idx) {
	char sIdx[64];
	snprintf(sIdx, sizeof(sIdx), "%d", _idx);
	CScriptVarLink *link = findChild(sIdx);
	if (link) {
		return link->m_var;
	} else {
		return new CScriptVar(TINYJS_BLANK_DATA, SCRIPTVAR_NULL); // undefined
	}
}

void CScriptVar::setArrayIndex(int32_t _idx, CScriptVar* _value) {
	char sIdx[64];
	snprintf(sIdx, sizeof(sIdx), "%d", _idx);
	CScriptVarLink *link = findChild(sIdx);
	if (link) {
		if (_value->isUndefined()) {
			removeLink(link);
		} else {
			link->replaceWith(_value);
		}
	} else {
		if (!_value->isUndefined()) {
			addChild(sIdx, _value);
		}
	}
}

int CScriptVar::getArrayLength() {
	int highest = -1;
	if (!isArray()) {
		return 0;
	}
	CScriptVarLink *link = m_firstChild;
	while (link) {
		if (isNumber(link->m_name)) {
			int val = atoi(link->m_name.c_str());
			if (val > highest) {
				highest = val;
			}
		}
		link = link->m_nextSibling;
	}
	return highest+1;
}

int CScriptVar::getChildren() {
	int n = 0;
	CScriptVarLink *link = m_firstChild;
	while (link) {
		n++;
		link = link->m_nextSibling;
	}
	return n;
}

int CScriptVar::getInt() {
	/* strtol understands about hex and octal */
	if (isInt()) {
		return m_intData;
	}
	if (isNull()) {
		return 0;
	}
	if (isUndefined()) {
		return 0;
	}
	if (isDouble()) {
		return (int)m_doubleData;
	}
	return 0;
}

double CScriptVar::getDouble() {
	if (isDouble()) {
		return m_doubleData;
	}
	if (isInt()) {
		return m_intData;
	}
	if (isNull()) {
		return 0;
	}
	if (isUndefined()) {
		return 0;
	}
	return 0; /* or NaN? */
}

const std::string &CScriptVar::getString() {
	/* Because we can't return a std::string that is generated on demand.
	 * I should really just use char* :) */
	static std::string s_null = "null";
	static std::string s_undefined = "undefined";
	if (isInt()) {
		char buffer[32];
		snprintf(buffer, sizeof(buffer), "%ld", m_intData);
		m_data = buffer;
		return m_data;
	}
	if (isDouble()) {
		char buffer[32];
		snprintf(buffer, sizeof(buffer), "%f", m_doubleData);
		m_data = buffer;
		return m_data;
	}
	if (isNull()) {
		return s_null;
	}
	if (isUndefined()) {
		return s_undefined;
	}
	// are we just a std::string here?
	return m_data;
}

void CScriptVar::setInt(int _val) {
	m_flags = (m_flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_INTEGER;
	m_intData = _val;
	m_doubleData = 0;
	m_data = TINYJS_BLANK_DATA;
}

void CScriptVar::setDouble(double _val) {
	m_flags = (m_flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_DOUBLE;
	m_doubleData = _val;
	m_intData = 0;
	m_data = TINYJS_BLANK_DATA;
}

void CScriptVar::setString(const std::string &_str) {
	// name sure it's not still a number or integer
	m_flags = (m_flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_STRING;
	m_data = _str;
	m_intData = 0;
	m_doubleData = 0;
}

void CScriptVar::setUndefined() {
	// name sure it's not still a number or integer
	m_flags = (m_flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_UNDEFINED;
	m_data = TINYJS_BLANK_DATA;
	m_intData = 0;
	m_doubleData = 0;
	removeAllChildren();
}

void CScriptVar::setArray() {
	// name sure it's not still a number or integer
	m_flags = (m_flags&~SCRIPTVAR_VARTYPEMASK) | SCRIPTVAR_ARRAY;
	m_data = TINYJS_BLANK_DATA;
	m_intData = 0;
	m_doubleData = 0;
	removeAllChildren();
}

bool CScriptVar::equals(CScriptVar *_v) {
	CScriptVar *resV = mathsOp(_v, LEX_EQUAL);
	bool res = resV->getBool();
	delete resV;
	return res;
}

CScriptVar *CScriptVar::mathsOp(CScriptVar *_b, int32_t _op) {
	CScriptVar *a = this;
	// Type equality check
	if (    _op == LEX_TYPEEQUAL
	     || _op == LEX_NTYPEEQUAL) {
		// check type first, then call again to check data
		bool eql = ((a->m_flags & SCRIPTVAR_VARTYPEMASK) == (_b->m_flags & SCRIPTVAR_VARTYPEMASK));
		if (eql) {
			CScriptVar *contents = a->mathsOp(_b, LEX_EQUAL);
			if (!contents->getBool()) {
				eql = false;
			}
			if (!contents->m_refs) {
				delete contents;
			}
		}
		if (_op == LEX_TYPEEQUAL) {
			return new CScriptVar(eql);
		} else {
			return new CScriptVar(!eql);
		}
	}
	// do maths...
	if (    a->isUndefined()
	     && _b->isUndefined()) {
		if (_op == LEX_EQUAL) {
			return new CScriptVar(true);
		} else if (_op == LEX_NEQUAL) {
			return new CScriptVar(false);
		} else {
			return new CScriptVar(); // undefined
		}
	} else if (    (a->isNumeric()  || a->isUndefined())
	            && (_b->isNumeric() || _b->isUndefined())) {
		if (!a->isDouble() && !_b->isDouble()) {
			// use ints
			int da = a->getInt();
			int db = _b->getInt();
			switch (_op) {
				case '+':        return new CScriptVar(da+db);
				case '-':        return new CScriptVar(da-db);
				case '*':        return new CScriptVar(da*db);
				case '/':        return new CScriptVar(da/db);
				case '&':        return new CScriptVar(da&db);
				case '|':        return new CScriptVar(da|db);
				case '^':        return new CScriptVar(da^db);
				case '%':        return new CScriptVar(da%db);
				case LEX_EQUAL:  return new CScriptVar(da==db);
				case LEX_NEQUAL: return new CScriptVar(da!=db);
				case '<':        return new CScriptVar(da<db);
				case LEX_LEQUAL: return new CScriptVar(da<=db);
				case '>':        return new CScriptVar(da>db);
				case LEX_GEQUAL: return new CScriptVar(da>=db);
				default:
					throw new CScriptException("Operation "+CScriptLex::getTokenStr(_op)+" not supported on the Int datatype");
			}
		} else {
			// use doubles
			double da = a->getDouble();
			double db = _b->getDouble();
			switch (_op) {
				case '+':        return new CScriptVar(da+db);
				case '-':        return new CScriptVar(da-db);
				case '*':        return new CScriptVar(da*db);
				case '/':        return new CScriptVar(da/db);
				case LEX_EQUAL:  return new CScriptVar(da==db);
				case LEX_NEQUAL: return new CScriptVar(da!=db);
				case '<':        return new CScriptVar(da<db);
				case LEX_LEQUAL: return new CScriptVar(da<=db);
				case '>':        return new CScriptVar(da>db);
				case LEX_GEQUAL: return new CScriptVar(da>=db);
				default:
					throw new CScriptException("Operation "+CScriptLex::getTokenStr(_op)+" not supported on the Double datatype");
			}
		}
	} else if (a->isArray()) {
		/* Just check pointers */
		switch (_op) {
			case LEX_EQUAL:  return new CScriptVar(a==_b);
			case LEX_NEQUAL: return new CScriptVar(a!=_b);
			default:
				throw new CScriptException("Operation "+CScriptLex::getTokenStr(_op)+" not supported on the Array datatype");
		}
	} else if (a->isObject()) {
		/* Just check pointers */
		switch (_op) {
			case LEX_EQUAL:  return new CScriptVar(a==_b);
			case LEX_NEQUAL: return new CScriptVar(a!=_b);
			default:
				throw new CScriptException("Operation "+CScriptLex::getTokenStr(_op)+" not supported on the Object datatype");
		}
	} else {
		std::string da = a->getString();
		std::string db = _b->getString();
		// use std::strings
		switch (_op) {
			case '+':        return new CScriptVar(da+db, SCRIPTVAR_STRING);
			case LEX_EQUAL:  return new CScriptVar(da==db);
			case LEX_NEQUAL: return new CScriptVar(da!=db);
			case '<':        return new CScriptVar(da<db);
			case LEX_LEQUAL: return new CScriptVar(da<=db);
			case '>':        return new CScriptVar(da>db);
			case LEX_GEQUAL: return new CScriptVar(da>=db);
			default:
				throw new CScriptException("Operation "+CScriptLex::getTokenStr(_op)+" not supported on the std::string datatype");
		}
	}
	assert(0);
	return 0;
}

void CScriptVar::copySimpleData(CScriptVar* _val) {
	m_data = _val->m_data;
	m_intData = _val->m_intData;
	m_doubleData = _val->m_doubleData;
	m_flags = (m_flags & ~SCRIPTVAR_VARTYPEMASK) | (_val->m_flags & SCRIPTVAR_VARTYPEMASK);
}

void CScriptVar::copyValue(CScriptVar* _val) {
	if (_val != nullptr) {
		copySimpleData(_val);
		// remove all current children
		removeAllChildren();
		// copy children of 'val'
		CScriptVarLink *child = _val->m_firstChild;
		while (child) {
			CScriptVar *copied;
			// don't copy the 'parent' object...
			if (child->m_name != TINYJS_PROTOTYPE_CLASS) {
				copied = child->m_var->deepCopy();
			} else {
				copied = child->m_var;
			}
			addChild(child->m_name, copied);
			child = child->m_nextSibling;
		}
	} else {
		setUndefined();
	}
}

CScriptVar* CScriptVar::deepCopy() {
	CScriptVar* newVar = new CScriptVar();
	newVar->copySimpleData(this);
	// copy children
	CScriptVarLink *child = m_firstChild;
	while (child) {
		CScriptVar *copied;
		// don't copy the 'parent' object...
		if (child->m_name != TINYJS_PROTOTYPE_CLASS) {
			copied = child->m_var->deepCopy();
		} else {
			copied = child->m_var;
		}
		newVar->addChild(child->m_name, copied);
		child = child->m_nextSibling;
	}
	return newVar;
}

void CScriptVar::trace(std::string _indentStr, const std::string& _name) {
	PARSERJS_WARNING(_indentStr << "'" << _name << "' = '" << getString() << "' " << getFlagsAsString());
	std::string indent = _indentStr+"\t";
	CScriptVarLink *link = m_firstChild;
	while (link != nullptr) {
		link->m_var->trace(indent, link->m_name);
		link = link->m_nextSibling;
	}
}

std::string CScriptVar::getFlagsAsString() {
	std::string flagstr = "";
	if (m_flags&SCRIPTVAR_FUNCTION) {
		flagstr += "FUNCTION ";
	}
	if (m_flags&SCRIPTVAR_OBJECT) {
		flagstr += "OBJECT ";
	}
	if (m_flags&SCRIPTVAR_ARRAY) {
		flagstr += "ARRAY ";
	}
	if (m_flags&SCRIPTVAR_NATIVE) {
		flagstr += "NATIVE ";
	}
	if (m_flags&SCRIPTVAR_DOUBLE) {
		flagstr += "DOUBLE ";
	}
	if (m_flags&SCRIPTVAR_INTEGER) {
		flagstr += "INTEGER ";
	}
	if (m_flags&SCRIPTVAR_STRING) {
		flagstr += "STRING ";
	}
	return flagstr;
}

std::string CScriptVar::getParsableString() {
	// Numbers can just be put in directly
	if (isNumeric()) {
		return getString();
	}
	if (isFunction()) {
		std::ostringstream funcStr;
		funcStr << "function (";
		// get list of parameters
		CScriptVarLink *link = m_firstChild;
		while (link) {
			funcStr << link->m_name;
			if (link->m_nextSibling) {
				funcStr << ",";
			}
			link = link->m_nextSibling;
		}
		// add function body
		funcStr << ") " << getString();
		return funcStr.str();
	}
	// if it is a std::string then we quote it
	if (isString()) {
		return getJSString(getString());
	}
	if (isNull()) {
		return "null";
	}
	return "undefined";
}

void CScriptVar::getJSON(std::ostringstream& _destination, const std::string _linePrefix) {
	if (isObject()) {
		std::string indentedLinePrefix = _linePrefix + "\t";
		// children - handle with bracketed list
		_destination << "{ \n";
		CScriptVarLink *link = m_firstChild;
		while (link != nullptr) {
			_destination << indentedLinePrefix;
			_destination	<< getJSString(link->m_name);
			_destination	<< " : ";
			link->m_var->getJSON(_destination, indentedLinePrefix);
			link = link->m_nextSibling;
			if (link != nullptr) {
				_destination	<< ",\n";
			}
		}
		_destination << "\n" << _linePrefix << "}";
	} else if (isArray()) {
		std::string indentedLinePrefix = _linePrefix + "\t";
		_destination << "[\n";
		int len = getArrayLength();
		if (len>10000) {
			len = 10000; // we don't want to get stuck here!
		}
		for (int32_t iii=0; iii<len; ++iii) {
			getArrayIndex(iii)->getJSON(_destination, indentedLinePrefix);
			if (iii<len-1) {
				_destination << ",\n";
			}
		}
		_destination << "\n" << _linePrefix << "]";
	} else {
		// no children or a function... just write value directly
		_destination << getParsableString();
	}
}


void CScriptVar::setCallback(JSCallback _callback, void *_userdata) {
	m_jsCallback = _callback;
	m_jsCallbackUserData = _userdata;
}

CScriptVar* CScriptVar::ref() {
	m_refs++;
	return this;
}

void CScriptVar::unref() {
	if (m_refs<=0) {
		PARSERJS_ERROR("OMFG, we have unreffed too far!");
	}
	if ((--m_refs)==0) {
		delete this;
	}
}

int CScriptVar::getRefs() {
	return m_refs;
}
