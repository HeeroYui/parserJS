/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#ifndef __PARSERJS_C_SCRIPT_EXCEPTION_H__
#define __PARSERJS_C_SCRIPT_EXCEPTION_H__

#include "tools.h"

class CScriptException {
	public:
		std::string m_text;
		CScriptException(const std::string &_exceptionText);
		const std::string& what() {
			return m_text;
		}
};


#endif
