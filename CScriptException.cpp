/**
 * @author Edouard DUPIN
 * 
 * @copyright 2011, Edouard DUPIN, all right reserved
 * 
 * @license APACHE v2.0 (see license file)
 */

#include "CScriptException.h"

CScriptException::CScriptException(const std::string &_exceptionText) :
  m_text(_exceptionText) {
  
}
